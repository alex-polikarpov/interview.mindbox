# Запрос №1 - итоговое решение

```
WITH cte AS (
	SELECT DISTINCT(o.ClientId) FROM Clients c
	JOIN Orders o
	ON c.Id = o.ClientId 
		AND o.DateOfFullPaymentUTC IS NOT NULL
		AND o.DateOfFullPaymentUTC <= CAST(DATEADD(day, 5, c.DateOfRegistrationUTC) AS DATETIME2(0))
)
SELECT Count(*) FROM cte
```

**Результаты для 1КК записей в каждой таблице**

 SQL Server Execution Times: CPU time = 1264 ms - 1320 ms, elapsed time = 393 ms - 400 ms.
 
 Почему выбрал этот запрос - Этот запрос выбрал, потому что время выполнения было меньше по сравнению с другими.

# Запрос №2 

```
WITH cte AS (
SELECT ROW_NUMBER() OVER (PARTITION BY ClientId ORDER BY DateOfFullPaymentUTC) AS RowNumber, o.*
FROM Orders o)
SELECT Count(*) FROM cte
JOIN Clients c
ON cte.RowNumber = 1 and c.Id = cte.ClientId
WHERE cte.DateOfFullPaymentUTC <= CAST(DATEADD(day, 5, c.DateOfRegistrationUTC) AS DATETIME2(0))
```

**Результаты для 1КК записей в каждой таблице**

 SQL Server Execution Times: CPU time = 1640 - 1850 ms,  elapsed time = 646 - 806 ms.

# Запрос №3

```
SELECT Count(c.Id)
FROM Clients c
JOIN 
	(SELECT ClientId, MIN(CAST(DATEADD(day, -5, DateOfFullPaymentUTC) AS DATETIME2(0))) DateFilter 
	FROM Orders o 
	WHERE DateOfFullPaymentUTC IS NOT NULL
	GROUP BY ClientId) AS o
ON c.Id = o.ClientId 
WHERE c.DateOfRegistrationUTC >= o.DateFilter
```

**Результаты для 1КК записей в каждой таблице**

SQL Server Execution Times: CPU time = 1485 - 1611 ms,  elapsed time = 454 - 481 ms.